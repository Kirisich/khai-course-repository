.model small
.stack 100h
.data
    msg db ''
.code

start:                                                   
    mov ax,@DATA
    mov ds,ax
    xor ax,ax
    int 33h     
    xor cx,cx        ;������������� �����
    mov     cx,7fh   ;����������� ��� �������
    mov     ax,0ch                                          
    push    cs                                              
    pop     es                                              
    mov     dx, offset mouseHandler ;��������� ����������
    int     33h                                             
    mov     ah, 0    ;������� ���� 
    int     16h                                             
    mov     ax, 000Ch ; ���� ��������� ����� ��� ������ �������, ��
    mov     cx, 0000h ; ��������� ������� �������                                      
    int     33h                                             
    mov		ax,4c00h                                            
    int		21h                                                 
 
mouseHandler proc                                          
    mov     ax, 0003h                                      
    int     33h     ;�������� ���� ��� ������
    mov bx, 0Ch
    test    bx, 00001100b  ;���� ����� ������, �� ������������� ����
    jnz     m                                             
    retf                                                    

m: 
    xor ax,ax   
    in  al, 61h     ;�o���ae� ��a�e��e �� �op�a b
    and al, 11111100b  ;o�����ae� ���a��� o� �a��epa
    xor ax,ax
    mov ax, 04a9h
                       ; ��, �� �����, ����� �������� ������������
                       ; ��������� ������� � �X
    mov al,10110110b   ; ���.��.�������: ����� 2, ����� 3, ��.�����
    out 43h,al         ; ������� � ������� ������
    mov ax,04a9h       ; DX:AX = 1193181
                       ; AX = (DX:AX) / �X
    out 42h,al         ; ���������� ������� ���� ��������
    mov al,ah
    out 42h,al         ; ���������� ������� ���� ��������
    in  al,61h         ; ���� PB
    or  al,11b         ; ������������� ���� 0-1
    out 61h,al         ; ���������� ������� � PB
    xor ax,ax
    mov ah, 01
    int 21h
    in  al,61h         ; ���� PB
    and al,not 11b     ; ���������� ���� 0-1
    out 61h,al 
    int 21h
    retf
mouseHandler endp  
    
exit:
	mov ax,4C00h
	int 21h
END start
 