masm 
model small 
stack 512 
.data 
per_a dw 4 
per_b dw 1 
per_c dw 4 
per_z dw 0 
.code 
main: 
mov ax, @data 
mov ds,ax 


;
in	al, 61h     ;пoлучaeм знaчeниe из пopтa b
	and	al, 11111100b  ;oтключaeм динaмик oт тaймepa



xor ax,ax

mov ax, 04a9h
         ; Да, на выход, чтобы избежать переполнения
                  ; Сохраняем частоту в СX
        mov al,10110110b   ; Упр.сл.таймера: канал 2, режим 3, дв.слово
        out 43h,al         ; Выводим в регистр режима
        mov ax,04a9h       ; DX:AX = 1193181
        	             ; AX = (DX:AX) / СX
        out 42h,al         ; Записываем младший байт счетчика
        mov al,ah
        out 42h,al         ; Записываем старший байт счетчика
        in  al,61h         ; Порт PB
        or  al,11b         ; Устанавливаем биты 0-1
        out 61h,al         ; Записываем обратно в PB

xor ax,ax
mov ah, 01
int 21h

in  al,61h         ; Порт PB
        and al,not 11b     ; Сбрасываем биты 0-1
        out 61h,al 
int 21h


exit: 
mov ax, 4c00h 
int 21h 
end main